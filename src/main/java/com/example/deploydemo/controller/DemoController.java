package com.example.deploydemo.controller;

import org.springframework.stereotype.Controller;

@Controller
public class DemoController {

    public String index() {
        return "index.html";
    }
}
